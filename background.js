// main.jsから呼び出される
let fileNames = {}
chrome.runtime.onMessage.addListener(function (msg) {
  const { url } = msg
  chrome.downloads.download({ url }).then(id => {
    fileNames[id] = "neko.png"
  })
})

// ファイル名変更のためリスナを追加(1回だけ登録)
chrome.downloads.onDeterminingFilename.addListener((ev, __suggest) => {
  const filename = fileNames[ev.id]
  delete fileNames[ev.id]
  __suggest({ filename })
})
